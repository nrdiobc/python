names = ['Dumbledore', 'Dobby', 'Ginny', 'Harry', 'Ron', 'Hermoine', 'Luna', 'Longbotom', 'Hagrid']

# For loop to print characters of string
for char in 'Dumbledore':
    print(char)

# For loop for printing list elments
for name in names:
    print(name.upper())

# # while loop for printing numbers 
i = 0 
while i <= 5:
    print(i)
    i = i+1

# # This is outside the while loop
# print('Final value of i = ' + str(i))