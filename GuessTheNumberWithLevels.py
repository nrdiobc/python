from random import randint
import time

counter = 0

print("Let's play number guessing game. You need to guess the secret number between 1 to 100 in 10 attempts.")

continuePlaying = True
upperRange = 100 
attempts = 10
while continuePlaying:

    # create a secret number between 1 to 100
    secretNumber = randint(1,upperRange)

    print('You have {} attempts'.format(attempts))
    
    while counter < attempts:
        counter += 1
        
        userNumber = int(input('Enter the code between 1 to {} :'.format(upperRange)))

        if userNumber < secretNumber:
            print('too small')
        elif userNumber > secretNumber:
            print('too big')
        elif userNumber == secretNumber:
            print('Let me check...')
            # sleep for 10 sec
            time.sleep(2)
            print('You cracked the code in {} attempts'.format(counter))

            choice = input('Do you want to continue playing (Yes/No) :')

            if choice != 'Yes' or choice != 'yes':
                continuePlaying = False
                upperRange += 100 

                if attempts > 6:
                    attempts -= 1

            break


            


