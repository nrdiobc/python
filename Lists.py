names = ['Dumbledore', 'Dobby', 'Ginny', 'Harry', 'Ron', 'Hermoine', 'Luna', 'Longbotom', 'Hagrid']


#get length of list
print(len(names))

# get sublist
print(names[0:3])

# get first element from list
print(names[0])

# change 0th element
names[0] = 'Professor'

# print list
print(names)