def add(a, b):
    print("I'm going to add a+ b " + str(a) + " " + str(b))
    return a + b

c = add(3, 5)
print(c)

d = add(5, 8)
print(d)

e = add(2, 3)
print(e)
