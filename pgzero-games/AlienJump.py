alien = Actor('alien', anchor=('middle', 'bottom'))

TITLE = "Alien Jump"
WIDTH = 500
HEIGHT = alien.height + 300
GROUND = HEIGHT - 10

# The initial position of the alien
alien.left = 0
alien.y = GROUND


def draw():
    screen.fill((0, 0, 0))
    alien.draw()


def update():
    # Move the alien around using the keyboard
    if keyboard.left:
        alien.x -= 4
    elif keyboard.right:
        alien.x += 4

    if keyboard.space:
        alien.y = GROUND - 100
        animate(alien, y=GROUND, tween='bounce_end', duration=.5)

    # If the alien is off the screen, move it back on screen
    if alien.right > WIDTH:
        alien.right = WIDTH
    elif alien.left < 0:
        alien.left = 0
