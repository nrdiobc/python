# string
car = 'BMW'
speed = '120km/hr'

# integer 
a = 10

#float
b = 2.5

print(a+b)
print(a-b)
print(a*b)
print(a/b)

# + is for concatination 
print(car + ' is going at ' + speed)
print(car + ' number ' + str(a))

# boolean variables
isDay = True
print(isDay)

isNight = False
print(isNight)
