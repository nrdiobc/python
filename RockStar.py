from random import randint

names = ['Dumbledore', 'Dobby', 'Ginny', 'Harry', 'Ron', 'Hermoine', 'Luna', 'Longbotom', 'Hagrid']

# element at 0th index
print(names[0])

# element at 2nd index
print(names[2])

# get random number between 0 and 8 including 0 and 8
index = randint(0, 8)

print('Best Wizard is :' + names[index])