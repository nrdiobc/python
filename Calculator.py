# get user input and convert it in int
number1 = int(input('Type number1 :'))

number2 = int(input('Type number2 :'))

# ask user if he want to add, subtract, multiply or divide two numbers
mathsOperation = input('Please enter either +,-,*,/ operation for these numbers :')

# Check for equiality
if mathsOperation == '+':
    print('number1 + number2 = ' + str(number1+number2))

# Check for unequiality
if mathsOperation != '+':
    print('number1 + number2 = ' + str(number1+number2))

# or condition
if mathsOperation == '+' or mathsOperation == '-' or mathsOperation == '*': 
    print('number1 + number2 = ' + str(number1+number2))

# if else 
if mathsOperation == '+': 
    print('number1 + number2 = ' + str(number1+number2))
else:
     print('number1 - number2 = ' + str(number1/number2))


#if else if 
if mathsOperation == '+':
    print('number1 + number2 = ' + str(number1+number2))
elif mathsOperation == '-':
    print('number1 - number2 = ' + str(number1-number2))
elif mathsOperation == '*':
    print('number1 * number2 = ' + str(number1*number2))
elif mathsOperation == '/':
    print('number1 / number2 = ' + str(number1/number2))
else:
    print('You entered invalid operaiton')
