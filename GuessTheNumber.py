from random import randint
import time

# create a secret number between 1 to 100
secretNumber = randint(1,100)

counter = 0

print("Let's play number guessing game. You need to guess the secret number between 1 to 100 in 10 attempts.")

while counter < 10:
    counter += 1

    userNumber = int(input('Enter the code :'))

    if userNumber < secretNumber:
        print('too small')
    elif userNumber > secretNumber:
        print('too big')
    elif userNumber == secretNumber:
        print('Let me check...')
        # sleep for 10 sec
        time.sleep(2)
        print('You cracked the code in {} attempts'.format(counter))
        break

print('You exausted 10 attempts! Try again later')
