largeString = 'This is lesson number 3'

# substring
print(largeString[0:7])

# length of string
print(len(largeString))

# upper string
print(largeString.upper())

# lower string
print(largeString.lower())

# endswith
print(largeString.endswith('3'))

# endswith
print(largeString.endswith('4'))

# startswith
print(largeString.startswith('This'))

# string replace
newString = largeString.replace('lesson', 'session')
print(newString)

